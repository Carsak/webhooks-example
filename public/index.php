<?php
error_reporting(E_ALL);

use Redmine\Client;
use Webhooks\Credential;
use Webhooks\Response;

require_once __DIR__ . "/../vendor/autoload.php";

$jsonRequest = file_get_contents('php://input');
$response = new Response($jsonRequest);
$issueId = $response->getIssueId();
$branchName = $response->getBranchName();
$actionName = $response->getActionName();

echo "Issue id is " . $issueId . PHP_EOL;
echo "BranchName is " . $branchName . PHP_EOL;
echo "ActionName is " . $actionName . PHP_EOL;

$credential = new Credential();
// Подключение к Redmine
$redmine = new Client('http://www.hostedredmine.com/', $credential->getBotApiKey());

// Можнно получить данные о задаче из Redmine
$issue = $redmine->issue->show($issueId);
$description = $issue['issue']['description'];
// Подготовим текст для issue description
$appendedText = PHP_EOL . "Был создан новый мерж реквест " . $response->getMergeRequestUrl() . PHP_EOL;

$errorFromRedmine = $redmine->issue->update($issueId, ['description' => $description . $appendedText]);

if ($errorFromRedmine) {
    echo "Ошибка при обновлении " . $errorFromRedmine;
} else {
    echo "Обновление прошло успешно";
}
