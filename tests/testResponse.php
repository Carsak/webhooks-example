<?php
use Webhooks\Response;
require_once __DIR__ . "/../vendor/autoload.php";

/**
 * Простой тест, не хотелось подключать целый PhpUnit для этого
 * Запуск php tests/testResponse.php
 */
$jsonRequest = require_once __DIR__ . "/jsonRequest.php";
$response = new Response($jsonRequest);

assert($response->getBranchName() === 'ma-851250-viewport', "Проверка, что из запроса корректно получено имя ветки");
assert($response->getActionName() === 'open', "Проверка, что из запроса корректно получено имя события/действия");
assert($response->getIssueId() === 851250, "Проверка, что корректно получено номер задачи из имени ветки");
assert($response->isMergeRequestOpened(), "Проверка, что событие вызвавшее веб хук это создание нового мерж реквеста");

echo 'Test passed' . PHP_EOL;